# some basic imports and setups
import cv2
import numpy as np
import tensorflow as tf
from alexnet import AlexNet
from caffe_classes import class_names


def predictor(imgs):
    imagenet_mean = np.array([104., 117., 124.], dtype=np.float32)

    # placeholder for input and dropout rate
    x = tf.placeholder(tf.float32, [1, 227, 227, 3])
    keep_prob = tf.placeholder(tf.float32)

    # create model with default config ( == no skip_layer and 1000 units in the last layer)
    model = AlexNet(x, keep_prob, 1000, [])

    # define activation of last layer as score
    score = model.fc8

    # create op to calculate softmax
    softmax = tf.nn.softmax(score)

    # Now we will start a TensorFlow session and load pretraine
    # d weights into the layer weights. Then we will loop over all images
    # and calculate the class probability for each image and plot the image again,
    # together with the predicted class and the corresponding class probability.
    saver = tf.train.Saver()
    with tf.Session() as sess:
        # Initialize all variables
        # saver.restore(sess, tf.train.latest_checkpoint('./model/'))
        sess.run(tf.global_variables_initializer())

        # Load the pretrained weights into the model
        model.load_initial_weights(sess)

        # Convert image to float32 and resize to (227x227)
        img = cv2.resize(imgs.astype(np.float32), (227, 227))

        # Subtract the ImageNet mean
        # img -= imagenet_mean

        # Reshape as needed to feed into model
        img = img.reshape((1, 227, 227, 3))

        # Run the session and calculate the class probability
        probs = sess.run(softmax, feed_dict={x: img, keep_prob: 1})

        # Get the class name of the class with the highest probability
        class_name = class_names[np.argmax(probs)]
        print class_name
        return 0


imgs = []
imgs = cv2.imread('cat.jpg')
imgs = cv2.resize(imgs,(277,277))
print imgs.shape
predictor(imgs)