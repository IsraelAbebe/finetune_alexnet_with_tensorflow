# Finetuned AlexNet with Tensorflow for age prediction

**Used Resources**

[blogpost](https://kratzert.github.io/2017/02/24/finetuning-alexnet-with-tensorflow.html)

[the code from github](https://github.com/kratzert/finetune_alexnet_with_tensorflow/tree/5d751d62eb4d7149f4e3fd465febf8f07d4cea9d)